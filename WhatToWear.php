<?php
require_once("./Products/ICloth.php");
require_once("./Fabrics/FabricInterface.php");

class WhatToWear {
    protected ICloth $hat;
    protected ICloth $jacket;
    protected ICloth $pants;
    protected ICloth $shoes;

    protected FabricInterface $fabrick;

    /**
     * При создании класса бизнес логики приложения, мы передаем ей одну из фабрик
     * для создания определенного вида продуктов
     */
    public function __construct(FabricInterface $fabrick){
        $this->fabrick = $fabrick;
        $this->createCloth();
    }

    /**
     * Создание продуктов, которые будут использоваться в бизнес логике. 
     * Каждая фабрика предоставляет нам свою собственную реализацию 
     */
    protected function createCloth(){
        $this->hat = $this->fabrick->createHat();
        $this->jacket = $this->fabrick->createJacket();
        $this->pants = $this->fabrick->createPants();
        $this->shoes = $this->fabrick->createShoes();
    }

    /**
     * Бизнес логика приложения, оставаясь в основном неизменной частью программы,
     * оперирует с абстрактными продуктами переданной ей фабрики
     */
    public function wearCloth(){        
        $this->hat->wear(); echo PHP_EOL;
        $this->jacket->wear(); echo PHP_EOL;
        $this->pants->wear(); echo PHP_EOL;
        $this->shoes->wear(); echo PHP_EOL;
    }

}
<?php
require_once("./Fabrics/Northfabrick.php");
require_once("./Fabrics/SouthFabric.php");
require_once("./WhatToWear.php");

//готовим одежду для поездки в холодные страны

echo "I going to North, what do i need to wear?", PHP_EOL;
$fabric = new Northfabrick();
$todayWear = new WhatToWear($fabric);
$todayWear->wearCloth();

echo PHP_EOL;

//а теперь, готовим одежду для теплых стран

echo "I going to South, what do i need to wear?", PHP_EOL;
$fabric = new SouthFabric();  // <-------- поменялся только класс фабрики, все остальное осталось без изменений
$todayWear = new WhatToWear($fabric); 
$todayWear->wearCloth();


/**
 * Логика использования приложения не изменилась, все части взаимодейтсвия остаются не затронутыми
 * Меняется только передаваемый класс фабрики, который меняет детали реализации приложения
 * Таким образом, можно не сильно беспокоится что логика приложения нарушится из-за смены реализации
 */
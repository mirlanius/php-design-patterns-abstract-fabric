<?php 
require_once("./Fabrics/FabricInterface.php");
require_once("./Products/ICloth.php");
require_once("./Products/NorthClothes/NorthHat.php");
require_once("./Products/NorthClothes/NorthJacket.php");
require_once("./Products/NorthClothes/NorthPants.php");
require_once("./Products/NorthClothes/NorthChoes.php");


class Northfabrick implements FabricInterface {
    public function createHat():ICloth{
        return new NorthHat();
    }
    public function createJacket():ICloth{
        return new NorthJacket();
    }
    public function createPants():ICloth{
        return new NorthPants();
    }
    public function createShoes():ICloth{
        return new NorthChoes();
    }


}
<?php 
require_once("./Products/ICloth.php");
require_once("./Products/SouthClothes/SouthHat.php");
require_once("./Products/SouthClothes/SouthJacket.php");
require_once("./Products/SouthClothes/SouthPants.php");
require_once("./Products/SouthClothes/SouthChoes.php");


class SouthFabric implements FabricInterface{

    public function createHat():ICloth{
        return new SouthHat();
    }
    public function createJacket():ICloth{
        return new SouthJacket();
    }
    public function createPants():ICloth{
        return new SouthPants();
    }
    public function createShoes():ICloth{
        return new SouthChoes();
    }

}
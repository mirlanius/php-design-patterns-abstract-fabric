<?php
interface FabricInterface {
    public function createHat();
    public function createJacket();
    public function createPants();
    public function createShoes();
}